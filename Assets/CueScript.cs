﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CueScript : MonoBehaviour {

    public GameObject GameBalls;
    public GameObject Table;
    public BallMover ballScript;

    void OnCollisionStay(Collision collision) // or void OnTriggerEnter(Collider other)
    {
        if (Table.GetComponentsInChildren<Collider>().Any(i => i.gameObject == collision.collider.gameObject) || GameBalls.GetComponentsInChildren<Collider>().Any(i => i.gameObject == collision.collider.gameObject))
        {
            ballScript.MoveCueUp();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
