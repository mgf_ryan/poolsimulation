﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PocketTrigger : MonoBehaviour
{
    AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        var body = other.GetComponent<Rigidbody>();
        body.velocity = new Vector3(0, 0, 0);
        body.angularVelocity = new Vector3(0, 0, 0);
        body.transform.position = new Vector3(-50, 3, 0);

        if (other.gameObject.tag == "Ball")
        {
            audio.volume = 1;
            audio.Play();

            if (other.name != "CueBall")
            {
                Destroy(other.gameObject);
                return;
            }
        }
    }
}
