﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallClick : MonoBehaviour
{
    AudioSource ballClick;
   // public AudioSource cueStrike;
    public BallMover ballScript;

    void Start()
    {
        ballClick = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 2 && collision.gameObject.tag == "Ball")
        {
            ballClick.volume = collision.relativeVelocity.magnitude / 200.0f;
            ballClick.Play();
        }

        if (collision.relativeVelocity.magnitude > 0.5 && collision.gameObject.tag == "Cue")
        {
            //cueStrike.volume = collision.relativeVelocity.magnitude / 200.0f;
            //cueStrike.Play();
            if (ballScript != null)
            {
                ballScript.ApplyContactSpin();
                ballScript.StopCueing();                
            }
        }
    }
}