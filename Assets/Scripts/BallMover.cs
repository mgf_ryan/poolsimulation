﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BallMover : MonoBehaviour
{

    private Rigidbody rb;
    private Rigidbody cueRb;
    public GameObject Cue;
    public GameObject CueRenderer;
    public GameObject GameBalls;

    private float currentLerpTime = 0.0F;
    private float lerpTime = 2.0F;
    private bool IsCueing = false;
    private bool IsSettingSpin = false;
    private bool IsMoving = false;
    private bool IsLerping = false;

    private Vector3 cuePos;
    private Vector3 cueStartPos;
    private Quaternion cueStartRotation;
    private float oldMousePos;
    private float oldMouseXPos;
    private float mousePos;
    private float mouseXPos;


    public float zAngleOffset = 0.0f;
    public float yAngleOffset = 0.0f;

    public void ApplyContactSpin()
    {
        Debug.Log(Camera.main.transform.right);
        var speed = cueRb.velocity.magnitude;
        var vel = cueRb.velocity;

        rb.AddTorque(Vector3.up * -speed/2 * zAngleOffset, ForceMode.VelocityChange);
        rb.AddTorque(Vector3.Cross(Vector3.up, cueRb.velocity).normalized * speed/2 * yAngleOffset, ForceMode.VelocityChange);
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cueRb = Cue.GetComponent<Rigidbody>();
        cueStartPos = Cue.transform.localPosition;
        cueStartRotation = Cue.transform.localRotation;
        Cue.SetActive(false);

        oldMousePos = Input.mousePosition.y;
        oldMouseXPos = Input.mousePosition.x;
    }

    Transform GetLowestBall()
    {
        Transform lowestFound = null;
        foreach (Transform obj in GameBalls.transform)
        {
            if (lowestFound == null || obj.gameObject.GetComponent<BallInfo>().BallNumber < lowestFound.gameObject.GetComponent<BallInfo>().BallNumber)
            {
                lowestFound = obj;
            }
        }

        return lowestFound;
    }

    void Update()
    {
        if (IsLerping)
        {
            currentLerpTime += Time.deltaTime;
            Vector3 pos = rb.transform.position;
            Vector3 target = Vector3.MoveTowards(pos, GetLowestBall().position, -70) + new Vector3(0, 20, 0);
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, target, currentLerpTime / lerpTime);

            var targetRotation = Quaternion.LookRotation(rb.transform.position - Camera.main.transform.position);
            // Smoothly rotate towards the target point.
            Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, targetRotation, currentLerpTime / lerpTime);

            if (currentLerpTime >= lerpTime || (Camera.main.transform.rotation == targetRotation && Camera.main.transform.position == target))
            {
                IsLerping = false;
                Camera.main.transform.position = target;
                Camera.main.transform.rotation = targetRotation;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                StopCueing();
                IsLerping = true;
                currentLerpTime = 0.0F;
            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                ToggleCueing();
            }


            IsSettingSpin = IsCueing && Input.GetKey(KeyCode.E);

            IsMoving = !IsCueing && !IsSettingSpin && Input.GetKey(KeyCode.M);

            oldMousePos = mousePos;
            oldMouseXPos = mouseXPos;
        }
    }

    private void StartCueing()
    {
        cueRb.transform.localPosition = cueStartPos;
        cueRb.transform.localRotation = cueStartRotation;
        cueRb.velocity = new Vector3(0, 0, 0);
        cueRb.angularVelocity = new Vector3(0, 0, 0);
        oldMousePos = Input.mousePosition.y;
        Cue.transform.right = Cue.transform.position - rb.transform.position;
        CueRenderer.transform.right = CueRenderer.transform.position - rb.transform.position;
        Cue.SetActive(true);
        zAngleOffset = 0;
        yAngleOffset = 0;

        IsCueing = true;
    }

    public void StopCueing()
    {
        Cue.SetActive(false);
        IsCueing = false;
    }

    private void ToggleCueing()
    {
        if (IsCueing)
        {
            StopCueing();
        }
        else
        {
            StartCueing();
        }
    }

    public void MoveCueUp()
    {
        HandleVerticalMove(-1f);
    }


    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        mousePos = Input.mousePosition.y;
        mouseXPos = Input.mousePosition.x;

        if (IsMoving)
        {
            rb.transform.Translate(moveVertical, 0, -moveHorizontal, Space.World);
        }
        else
        {
            Camera.main.transform.RotateAround(rb.transform.position, Vector3.up, moveHorizontal);

            HandleVerticalMove(moveVertical);
        }

        if (IsCueing)
        {
            Cue.transform.right = (Cue.transform.position - rb.transform.position);
            CueRenderer.transform.right = (CueRenderer.transform.position - rb.transform.position);

            // apply spin offset to the renderer only
            CueRenderer.transform.Rotate(Vector3.up, zAngleOffset, Space.World);
            var sideDirection = Vector3.Cross(CueRenderer.transform.right, Vector3.up);
            CueRenderer.transform.Rotate(sideDirection, -yAngleOffset, Space.World);
            cueRb.angularVelocity = new Vector3(0, 0, 0);

            if (IsSettingSpin)
            {
                zAngleOffset += ((mouseXPos - oldMouseXPos) / 50);
                zAngleOffset = Mathf.Abs(zAngleOffset) < 1.65f ? zAngleOffset : ((zAngleOffset < 0) ? -1.65f : 1.65f);

                yAngleOffset += (mousePos - oldMousePos) / 50;
                yAngleOffset = Mathf.Abs(yAngleOffset) < 1.65f ? yAngleOffset : ((yAngleOffset < 0) ? -1.65f : 1.65f);
            }
            else
            {
                cuePos = Cue.transform.position;
                cueRb.velocity = Vector3.Lerp(cueRb.velocity, Cue.transform.right * (mousePos - oldMousePos) * -10, 0.3F);

                if (oldMouseXPos != 0.0f)
                    Camera.main.transform.RotateAround(rb.transform.position, Vector3.up, (mouseXPos - oldMouseXPos)/200);
            }
        }
    }

    private void HandleVerticalMove(float moveVertical)
    {
        if (moveVertical != 0.0f && !(moveVertical > 0.0f && Camera.main.transform.position.y <= 0.0f))
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - moveVertical, Camera.main.transform.position.z);
            Camera.main.transform.LookAt(rb.transform);
            Cue.transform.right = Cue.transform.position - rb.transform.position;
            CueRenderer.transform.right = CueRenderer.transform.position - rb.transform.position;
        }
    }
}
