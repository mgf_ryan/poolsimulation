﻿using UnityEngine;
using System.Collections;

public class BallDrag
    : MonoBehaviour
{
    private Rigidbody rb;
    public float BallDragFactor;
    public float ConstantDragFactor;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 3000.0f;
    }

    void FixedUpdate()
    {
        rb.angularVelocity *= (ConstantDragFactor * (1 - 1 / (1 + BallDragFactor * rb.angularVelocity.magnitude)));
    }
}
